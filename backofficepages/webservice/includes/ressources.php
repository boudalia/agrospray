<?php

/** Classe de gestion des ressources texte externe 
   * @desc Classe de gestion des ressources texte externe  en PHP 5.x. Utilise le fichier XML pass� dans le constructeur sinon essaye de charger le fichier ressources.xml dans le r�pertoire includes
   * @package -- Standard Class
   * @author  F.Diouri
   * @copyright 
   * @version 3 PHP5
   * 
   **/


class ressources extends DOMDocument{
        static  $instance;
        public function __construct($filename="") {
                if ($filename=="") { $filename = "language/lang-fr.xml";}
                if (!file_exists($filename)) {
                          throw new Exception("Fatal Error on Ressource file : unable to load ressources : ".$filename);
                }
                          try {
                              parent::load($filename);
                          } catch (Exception $e ) {
                              throw new Exception("Fatal Error on Ressource file : ".$e->getLine() . $this->parseError->reason);
                          }
        }
             
        /**
        * Renvoit l'instance du singleton de ressource
        *
        * @return  instance
        */
        public static function getInstance($filename="") {
            if (empty(self::$instance)) {
             self::$instance = new ressources($filename);  
           }
          return self::$instance;
        }
  /**
   *Identique � getRessource
   *@return string 
   *@access public
   **/           
  public function __get($id) {
    return $this->getRessource($id);
  }
        
        /** Renvoie le texte d'erreur associ� � un identifiant
          * @param string id : identifiant de ressource cherch�
        * @return  string la valeur ou ##id## quand il n'est pas trouv�
         * @access public
    **/
     public function getRessource($id) {
          $xp = new domxpath($this);
                $elts = $xp->query("/lang/ressource[@id='$id']");
                //should have only one
                if ($elts->length != 0) {
                        foreach($elts as $node){
                                return utf8_decode($node->getAttribute("value"));
                        }
                } else {
                        return ("$id");
                }
                return "Not Found";
      }
      
      /** Renvoie le texte d'erreur associ� � une table/un attribut
              * @param Nom de la Table (string)
              * @param Nom de l'attribut (string)
            * @return  string ou  erreur syst�me S0000
             * @access public
             **/
      public function getErrorMsg($sTable,$sField) {
              $xp = new domxpath($this);
                $elts = $xp->query("/lang/ressource[@table='$sTable' and @field='$sField']");
                //should have only one
                if ($elts->length != 0) {
                        foreach($elts as $node){
                                return $node->getAttribute("value");
                        }
                } else {
                        return ($this->getRessource("S0000")." (".$sTable." - ".$sField.")");
                }
                return "Not Found";
      }
        
}

?>
