<?php
class MySQLExeption extends Exception{
public function __construct($Msg)
 {
 parent :: __construct($Msg);
 }

public function RetourneErreur() {
$msg = '<div><strong>' . $this->getMessage() . '</strong>';
$msg .= ' Ligne : ' . $this->getLine() . '</div>';
$msg .= ' Ligne : ' . $this->getCode() . '</div>';
return $msg;

}

}

class Mysql
{


private
$Serveur = '',
$Bdd = '',
$Identifiant = '',
$Mdp = '',
$Lien = '',
$Debogue = true,
$NbRequetes = 0;
 
/**
  * Constructeur de la classe
  * Connexion aux serveur de base de donn�e et s�lection de la base
  *
  * $Serveur = L'h�te (ordinateur sur lequel Mysql est install�)
  * $Bdd = Le nom de la base de donn�es
  * $Identifiant = Le nom d'utilisateur
  * $Mdp = Le mot de passe
  */
public function __construct()
{


$this->Serveur = HOSTLOCAL;
$this->Bdd = BDD;
$this->Identifiant = USERBDD;
$this->Mdp = PASSBDD;
$this->Lien=mysqli_connect($this->Serveur, $this->Identifiant, $this->Mdp);
 
if(!$this->Lien && $this->Debogue)
throw new MySQLExeption('Erreur de connexion au serveur MySql!!!');
 
$Base = mysqli_select_db($this->Lien,$this->Bdd);
 
if (!$Base && $this->Debogue)
throw new MySQLExeption('Erreur de connexion � la base de donnees!!!');
}
 
/**
  * Retourne le nombre de requ�tes SQL effectu� par l'objet
  */
public function RetourneNbRequetes()
{
return $this->NbRequetes;
}
 
/**
  * Envoie une requ�te SQL et r�cup�re le r�sult�t dans un tableau pr� format�
  *
  * $Requete = Requ�te SQL
  */
public function TabResSQL($Requete)
{
$i = 0;
$Ressource = mysqli_query($this->Lien,$Requete);
$TabResultat=array();
if (!$Ressource and $this->Debogue) throw new MySQLExeption('Erreur de requ�te SQL!!!'."\n". mysqli_error());
while ($Ligne = mysqli_fetch_assoc($Ressource))
{
foreach ($Ligne as $clef => $valeur) $TabResultat[$i][$clef] = $valeur;
$i++;
}
mysqli_free_result($Ressource);
$this->NbRequetes++;
return $TabResultat;
}
 
/**
  * Retourne le dernier identifiant g�n�r� par un champ de type AUTO_INCREMENT
  *
  */
public function DernierId()
{
return mysqli_insert_id($this->Lien);
}
 
/**
  * Envoie une requ�te SQL et retourne le nombre de table affect�
  *
  * $Requete = Requ�te SQL
  */
public function ExecuteSQL($Requete)
{
$Ressource = mysqli_query($this->Lien,$Requete);
if (!$Ressource and $this->Debogue) throw new MySQLExeption('Erreur de requ�te SQL!!!'."\n". mysqli_error());
$this->NbRequetes++;
$NbAffectee = mysqli_affected_rows($this->Lien);
return $NbAffectee;
}
}

?>