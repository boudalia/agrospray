<?php
@ini_set('display_errors', 'on');
date_default_timezone_set('UTC');
  include 'includes/application_top.php';
  if(!empty($_SESSION['admin']) or !empty($_SESSION['membre']))
  {
  
  
  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Agro Spray Technic</title>
<script type="text/javascript" language="javascript" src="script.js"></script>
<link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
<!--script src="js/cufon-yui.js" type="text/javascript"></script->
		<script src="js/helveticalight_275.font.js" type="text/javascript"></script>
		<script src="js/helveticathin_250.font.js" type="text/javascript"></script-->
		<script type="text/javascript">
           /* Cufon.replace('#header', { fontFamily: 'helveticathin' });
            Cufon.replace('.menu', { fontFamily: 'helveticathin' });
            Cufon.replace('h2', { fontFamily: 'helveticathin' });
            Cufon.replace('.s-menu', { fontFamily: 'helveticathin' });*/
		</script>
<link rel="stylesheet" type="text/css" href="css/default.css" />
<?php
// if(FCKEDITOR)
// {   include 'fckeditor/header_fck.php';}
?>
<title>Administration</title>
<script src="../js/jquery.js"></script>
<style type="text/css" title="currentStyle">
			@import "../media/css/demo_table_jui.css";
			/*@import "../examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css";*/
		</style>

<script src="../js/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" href="../css/jquery.lightbox-0.5.css" type="text/css" media="screen" />



<script type="text/javascript">
   /* $(function() {
        $('.photo a').lightBox();
    });*/
    </script>
<script type="text/javascript" language="javascript" src="../media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
    function selectall(mm){
        if(mm.checked){
            $(":checkbox").not(".fonct").attr('checked','checked');

        }else{

            $(":checkbox").not(".fonct").removeAttr('checked');
        }
    }
			$(document).ready(function() {
				oTable = $('#example').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"iDisplayLength": 25,
					"oLanguage": {
						"sProcessing":     "Traitement en cours...",
						"sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
						"sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
						"sInfo":           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
						"sInfoEmpty":      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
						"sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
						"sInfoPostFix":    "",
						"sSearch":         "Rechercher&nbsp;:",
						"sLoadingRecords": "Téléchargement...",
						"sUrl":            "",
						"oPaginate": {
							"sFirst":    "Premier",
							"sPrevious": "Pr&eacute;c&eacute;dent",
							"sNext":     "Suivant",
							"sLast":     "Dernier"
						}
					}
				});				
				oTable = $('#example1').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"aaSorting": [[ 0, "desc" ]],
					"iDisplayLength": 25,
					"oLanguage": {
						"sProcessing":     "Traitement en cours...",
						"sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
						"sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
						"sInfo":           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
						"sInfoEmpty":      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
						"sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
						"sInfoPostFix":    "",
						"sSearch":         "Rechercher&nbsp;:",
						"sLoadingRecords": "Téléchargement...",
						"sUrl":            "",
						"oPaginate": {
							"sFirst":    "Premier",
							"sPrevious": "Pr&eacute;c&eacute;dent",
							"sNext":     "Suivant",
							"sLast":     "Dernier"
						}
					}
				});			
				$('.succ').show('slow');
				
				setInterval(function() {
					$('.succ').hide('slow');
				}, 2000);				
				
			} );
</script>
		<link type="text/css" href="css/south-street/jquery-ui-1.8.22.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="js/jquery-ui-1.8.22.custom.min.js"></script>
		<script>
	$(function() {
		$( "#datepicker" ).datepicker({
			defaultDate: "+1w",
			showOn: "button",
			buttonImage: "images/calendar.gif",
			buttonImageOnly: true,
			changeMonth: true,
			dateFormat: "dd/mm/yy",
			numberOfMonths: 1,
			onSelect: function( selectedDate ) {
				$( "#datepicker2" ).datepicker( "option", "minDate", selectedDate );
			}
		});
		$( "#datepicker2" ).datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			showOn: "button",
			buttonImage: "images/calendar.gif",
			buttonImageOnly: true,
			dateFormat: "dd/mm/yy",
			numberOfMonths: 1,
			onSelect: function( selectedDate ) {
				$( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
			}
		});	
		$.datepicker.regional['fr'] = {clearText: 'Effacer', clearStatus: '',
		closeText: 'Fermer', closeStatus: 'Fermer sans modifier',
		prevText: '<Préc', prevStatus: 'Voir le mois précédent',
		nextText: 'Suiv>', nextStatus: 'Voir le mois suivant',
		currentText: 'Courant', currentStatus: 'Voir le mois courant',
		monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
		'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
		'Jul','Aoû','Sep','Oct','Nov','Déc'],
		monthStatus: 'Voir un autre mois', yearStatus: 'Voir un autre année',
		weekHeader: 'Sm', weekStatus: '',
		dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
		dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
		dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
		dayStatus: 'Utiliser DD comme premier jour de la semaine', dateStatus: 'Choisir le DD, MM d',
		dateFormat: 'dd/mm/yy', firstDay: 0, 
		initStatus: 'Choisir la date', isRTL: false};
	$.datepicker.setDefaults($.datepicker.regional['fr']);
	});
	</script>
	<link rel="stylesheet" href="css/colorbox.css" />
		<script src="js/jquery.colorbox.js"></script>
		<script>
			$(document).ready(function(){
				$(".iframe").colorbox({iframe:true, width:"50%", height:"75%"});
				$(".realisa").colorbox({iframe:true, width:"90%", height:"80%"});

            });
		</script>

    <?php
 if(isset($_SESSION['profil']) && $_SESSION['profil']=="DCNORD" or  $_SESSION['profil']=="DCSUD" or $_SESSION['profil']=="logistique" or $_SESSION['profil']=="tresorier"
 or $_SESSION['profil']=="ADV" or $_SESSION['profil']=="ADVSUD"){
        ?>

        <script>
            $(document).ready(function(){



                var interval;
                function callAjax() {
                    $.ajax({
                        type: 'POST',
                        url: 'ajax_comment.php',
                        cache: false,
                        success: function (data) {
                            $('#commentjax').html(data);// first set the value
                            interval = setTimeout(callAjax, 15000);

                            console.log("comment")
                        }
                    });
                }
                callAjax();
                /*   setTimeout(function(){$("#commentjax").hide(500)}, 9000);*/

            });
        </script>
    <?php
}
    ?>
</head>
<body onload="menuSlider.init('menu','slide');init()">
<div id="commentjax"></div>
<div id="header">
<a href="."><img src="images/logo.jpg" height="70" width="180" style="padding-left:10px;padding-top:3px;"/></a>
    <div id="info">
	<?php
		$sqk=mysql_query('select * from users where id_membre="'.$_SESSION['id_membre'].'"');
		$dvy=mysql_fetch_array($sqk);
	?>
    <div class="info" style="color:#313131">Bonjour, <?php echo $dvy['nom'].' '.$dvy['prenom'];?><br ><?php  echo $_SESSION['profil'];?></div>
       <div class="compte"><a href="index.php?action=espace_membre_modifier&id_membre=<?php echo $_SESSION['id_membre']; ?>">Mon compte</a></div>
       <div class="outils"><a href="#">Paramètres</a></div>
        <div class="log-off" onclick="document.location.href='identification.php?logoff'"><a href="identification.php?logoff">Déconnexion</a></div>
    </div>
</div>
<div id="menu">
    <div class="menu">
      <ul>
      	<?php if(isset($_SESSION['profil']) && $_SESSION['profil']=="achats" or $_SESSION['profil']=="ventes" ) { ?>
      	 <li <?php if(preg_match('/^dashboard/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?>><a href="index.php?action=dashboard">Dashbord</a></li>

            <li <?php if(preg_match('/^catalogue/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=catalogue&language=fr">Catalogue</a></li>

            <li <?php if(preg_match('/^espace_client/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=espace_client">Clients</a></li>

            <li <?php if(preg_match('/^pushandroid/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=pushandroid">Push</a></li>
            <?php if(isset($_SESSION['profil']) && $_SESSION['profil']=="achats" ) { ?>
            <li <?php if(preg_match('/^tva/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=tva">TVA</a></li>
            <?php 
			}
			?>
            <li <?php if(preg_match('/^commandes/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=commandes">Commandes</a></li>
			<?php 
			}
			?>


			<?php 	
			if(isset($_SESSION['profil']) && $_SESSION['profil']=="administrateur" or $_SESSION['profil']=="ADVSUD"  or $_SESSION['profil']=="ADV" ) { ?>
            <li <?php if(preg_match('/^dashboard/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?>><a href="index.php?action=dashboard">Dashbord</a></li>
            <li <?php if(preg_match('/^catalogue/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=catalogue&language=fr">Catalogue</a></li>

      		<li <?php if(preg_match('/^espace_client/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=espace_client">Clients</a></li>
	<?php if(isset($_SESSION['profil']) && $_SESSION['profil']=="administrateur") { ?>
           <li <?php if(preg_match('/^espace_membre/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=espace_membre">Commerciaux</a></li>
			<li <?php if(preg_match('/^users/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=users">Utilisateurs</a></li>
			<li <?php if(preg_match('/^pushandroid/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=pushandroid">Push</a></li>
				<li <?php if(preg_match('/^tva/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=tva">TVA</a></li>
	<?php } ?>
			<li <?php if(preg_match('/^commandes/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=commandes">Commandes</a></li>
			<?php
			  $adv = array("ADV");
			  $advsud = array("ADVSUD");
              if (in_array($_SESSION['profil'], $adv)) {
			$req='select * from  commandes c ,cms_v2_membres l  where c.lu=1 and c.fk_commercial=l.id_membre  and l.nord=1 and c.etat_cmd!=2';
			}else if (in_array($_SESSION['profil'], $advsud)) {
			$req='select * from  commandes c ,cms_v2_membres l  where c.lu=1 and c.fk_commercial=l.id_membre  and l.sud=1 and c.etat_cmd!=2';
			}else{
			$req='select * from  commandes where lu=1 and etat_cmd!=2';
			}
			$de=mysql_query($req);
			$nu=mysql_num_rows($de);
			if($nu>0){ ?>
			<li> <img  style="margin-top: -8px;width: 40px;" src="images/notif.png" /><span style="  margin-left: -35px;
position: absolute;
    width: 27px;"><a href="#"><?php echo $nu;?></a></span></li>
<?php }  } 
			if(isset($_SESSION['profil']) && $_SESSION['profil']=="logistique" || $_SESSION['profil']=="transport" || $_SESSION['profil']=="transportSud" ) { ?>
            <li <?php if(preg_match('/^dashboard/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?>><a href="index.php?action=dashboard">Dashbord</a></li>
			<?php if(isset($_SESSION['profil']) && $_SESSION['profil']=="logistique") { ?>
                       <li <?php if(preg_match('/^espace_membre/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=espace_membre">Commerciaux</a></li>
			      		<li <?php if(preg_match('/^espace_client/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=espace_client">Clients</a></li>
						<li <?php if(preg_match('/^catalogue/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=catalogue&language=fr">Catalogue</a></li>
                    <?php if(isset($_SESSION['profil']) && $_SESSION['profil']!="logistique") { ?>
                     <li <?php if(preg_match('/^users/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=users">Utilisateurs</a></li>	<?php } ?>
                   <li <?php if(preg_match('/^tva:',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=tva">TVA</a></li>
							<?php } ?>
            <li <?php if(preg_match('/^commandes/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=commandes">Commandes</a></li>
	<?php
	  $tras = array("transportSud","transport");
      if (in_array($_SESSION['profil'], $tras)) {
	$req='select * from  commandes where vport=0 and etat_cmd=10';
	}else{
	$req='select * from  commandes where vsl=0 and etat_cmd=8';
	}
			
			$de=mysql_query($req);
			$dat=mysql_fetch_array($de);
// echo $req;
			$nu=mysql_num_rows($de); 
			// $nu=0;
			if($nu>0){ ?>
			<li> <img  style="margin-top: -8px;width: 40px;" src="images/notif.png" /><span style="  margin-left: -35px;
position: absolute;
    width: 27px;"><a href="#"><?php echo $nu;?></a></span></li>
            <?php }  }

                $droit = array("DCNORD", "DCSUD", "tresorier");
                $dc = array("DCNORD", "DCSUD");
                if (in_array($_SESSION['profil'], $droit)) {
                ?>
            <li <?php if(preg_match('/^dashboard/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?>><a href="index.php?action=dashboard">Dashbord</a></li>
            <li <?php if(preg_match('/^espace_client/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=espace_client">Clients</a></li>
                    <?php if(isset($_SESSION['profil']) && $_SESSION['profil']!="tresorier" ) { ?>
                       <li <?php if(preg_match('/^catalogue/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=catalogue&language=fr">Catalogue</a></li>
                    <?php } ?>
                    <li <?php if(preg_match('/^commandes/',$_SERVER['REQUEST_URI'])) { echo 'value="1"';} ?> class="sep"><a href="index.php?action=commandes">Commandes</a></li>
	<?php
              if($_SESSION['profil']=="DCNORD") $sec='';
              if($_SESSION['profil']=="DCSUD") $sec='';
              if($_SESSION['profil']=="tresorier") $sec='';
                    $ledc = array("DCNORD", "DCSUD");
                    if (in_array($_SESSION['profil'], $ledc)) {
                        $req='select * from  commandes where dc=1';

                    }else{
                        $req='select * from  commandes where vtr=0 and  etat_cmd = 6 ';
                    }



			$de=mysql_query($req);
			$dat=mysql_fetch_array($de);

			$nu=mysql_num_rows($de);
		  if (in_array($_SESSION['profil'], $dc)) {
		  $nu=0;
		  }
			if($nu>0){ ?>
			<li> <img  style="margin-top: -8px;width: 40px;" src="images/notif.png" /><span style="  margin-left: -35px;
position: absolute;
    width: 27px;"><a href="#"><?php echo $nu;?></a></span></li>
<?php }  } ?>

        </ul>
        <div id="slide"><!-- --></div>
    </div>
</div>
<?php 
$sql='select * from commandes c ,  comments o where c.id_cmd=o.fk_cmd and c.etat_cmd=5 and o.fk_user=11 order by  c.id_cmd desc limit 1';
  $re=mysql_query($sql) or die(mysql_error());
  $com=mysql_fetch_array($re);
 /* echo '<div style="   background: none repeat scroll 0 0 #ECC4D3;
    border-radius: 6px 6px 6px 6px;
    float: right;
    margin-top: 0px;
    padding: 4px;
    position: relative;
    width: 384px;"><span style="font-size:8px;">Dèrnier message:</span><br/> N° CMD:<b> '.$com['id_cmd'].'</b> ('.$com['comment'].')</div>';
 */ ?>
<div id="content">
<table width="100%">
	<tr><td width="16%" valign="top">
        <div class="s-menu">
             <div id="sous-menu"></div>
             <div id="sous-menu1">
                  <ul>
				  <?php
				    $droit = array("administrateur","achats","ventes");
				    // $droit = array("tresorier", "logistique");
					    if (in_array($_SESSION['profil'], $droit)) {
					if($_GET['action']=='catalogue' || preg_match('/^catalogue/',$_SERVER['REQUEST_URI'])) {
                      
                    
                            echo '<li><a href="index.php?action=catalogue">Produits </a> </li>';
                            echo '<li><a href="index.php?action=categories">Familles de produits</a> </li>';
                            echo '<li><a href="index.php?action=catalogue_add_product">Ajouter un produit </a> </li>';
                            echo '<li><a href="index.php?action=catalogue_add_categorie">Ajouter une famille </a> </li>';
                   
					}elseif ($_GET['action']=='espace_membre' or $_GET['action']=='espace_membre_add') {
						echo '<li><a href="index.php?action=espace_membre">Liste des commerciaux</a> </li>';
						echo '<li><a href="index.php?action=espace_membre_add">Ajouter un commercial</a> </li>';
					}elseif ($_GET['action']=='espace_client' or $_GET['action']=='espace_client_add') {
						echo '<li><a href="index.php?action=espace_client">Liste des clients</a> </li>';
						echo '<li><a href="index.php?action=espace_client_add">Ajouter un client</a> </li>';
                        echo '<li><a href="index.php?action=upload">Upload clients</a> </li>';
					}elseif ($_GET['action']=='users' or $_GET['action']=='espace_client_add') {
						echo '<li><a href="index.php?action=users">Liste des utilisateurs</a> </li>';
						echo '<li><a href="index.php?action=users_add">Ajouter un utilisateur</a> </li>';
					}elseif ($_GET['action']=='categories') {
						echo '<li><a href="index.php?action=catalogue_add_product">Ajouter un produit</a> </li>';
						echo '<li><a href="index.php?action=catalogue_add_categorie">Ajouter une famille</a> </li>';
					}
					 }
					 	$droit2 = array("administrateur", "logistique", "ADV", "ADVSUD","ventes");
					    if (in_array($_SESSION['profil'], $droit2)) {
					 if($_GET['action']=='commandes') {
						echo '<li><a href="index.php?action=commandes">Commandes en cours</a> </li>';
						echo '<li><a href="index.php?action=commandes&arch">Archive</a> </li>';
						if(isset($_SESSION['profil']) && $_SESSION['profil']!="transport" && $_SESSION['profil']!="transportSud"){
						echo '<li><a href="index.php?action=commandes&annul">Commandes annulées</a> </li>';
						echo '<li><a href="index.php?action=commandes&rate">Commandes ratées</a> </li>';
						}
					}
					}
					    
				  ?>
                  </ul>
             </div>
             <div id="sous-menu2"></div>
         </div>
	</td>
	<td width="83%" valign="top">
		<div id="contenu">
			<?php include 'includes/container.php'; ?>
    	</div>
	</td></tr>
</table>
</div>
<script type="text/javascript" src="js/soundjs-NEXT.combined.js"></script>

<script>


        var preload;

        function init() {
            if (window.top != window) {
                document.getElementById("header").style.display = "none";
            }

            if (!createjs.Sound.initializeDefaultPlugins()) {
                document.getElementById("error").style.display = "block";
                document.getElementById("content").style.display = "none";
                return;
            }

          //  document.getElementById("loader").className = "loader";
            var assetsPath = "";
            var manifest = [
                {src:"js/Pop.mp3", id:1}
            ];

            createjs.Sound.alternateExtensions = ["mp3"];	// add other extensions to try loading if the src file extension is not supported
            createjs.Sound.addEventListener("fileload", createjs.proxy(soundLoaded, this)); // add an event listener for when load is completed
            createjs.Sound.registerManifest(manifest, assetsPath);
        }

        function soundLoaded(event) {
        //    document.getElementById("loader").className = "";
           /* var div = document.getElementById(event.id);
            div.style.backgroundImage = "url('assets/audioButtonSheet.png')";*/
        }

        function stop() {
            if (preload != null) { preload.close(); }
            createjs.Sound.stop();
        }


        function playSound(id) {
            //Play the sound: play (src, interrupt, delay, offset, loop, volume, pan)
            var instance = createjs.Sound.play(id, createjs.Sound.INTERRUPT_NONE, 0, 0, false, 1);
            if (instance == null || instance.playState == createjs.Sound.PLAY_FAILED) { return; }

            instance.addEventListener ("complete", function(instance) {

            });
        }
</script>
</body>
</html>

<?php
}else{
header("location:identification.php");
}?>