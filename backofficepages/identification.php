<?php
date_default_timezone_set('UTC');
  session_start();
  
	  if(isset($_GET['logoff'])) {
      unset($_SESSION['admin']);
       unset($_SESSION['profil']);
      unset($_SESSION['id_membre']);    unset($_SESSION['membre']);
	  }
      
  include '../includes/config.php';
  
  
 


    if(isset($_POST['LOGIN_FORM_ENVOYER']))
    {
	
	  $connexion = mysqli_connect(SQL_SVR,SQL_LOGIN,SQL_PASS) or die ("Connexion au serveur impossible");   
    
        mysqli_select_db($connexion,SQL_DATABASE) or die(mysqli_error());
      $sql='select * from users where email="'.$_POST['FORM_LOGIN'].'" and password="'.$_POST['FORM_PASSWORD'].'" and statut=1';
    
	  $res=mysqli_query($connexion,$sql);

	  $nm=mysqli_num_rows($res);
	 
      if($nm==1)
			{		
				
				$ro=mysqli_fetch_array($res);
				  $_SESSION['id_membre']=$ro['id_membre'];
				   $_SESSION['profil']=$ro['profil'];
				   $_SESSION['zone']=$ro['zone'];
				  // on met  jour derniere conncection
				  $sql='update users set date_login="'.date('Y-m-d').'" where id_membre="'.$_SESSION['id_membre'].'"';
				  mysqli_query($connexion,$sql);
				  
				  $_SESSION['membre']=true;
				
				  header('LOCATION:index.php?action=dashboard');
			

		  
		}elseif(($_POST['FORM_LOGIN']==LOGIN_ADMIN && md5($_POST['FORM_PASSWORD'])==PASSWORD_ADMIN)||($_POST['FORM_LOGIN']==LOGIN_SUPER_ADMIN && md5($_POST['FORM_PASSWORD'])==PASSWORD_SUPER_ADMIN))
        {
            $_SESSION['admin']=true;
            header('LOCATION:index.php?action=dashboard');
			
        }
        else
        {
            
            unset($_SESSION['admin']);
            unset($_SESSION['membre']);
			$erreur='Erreur sur l\'identifiant et/ou le mot de passe';
			
        }
        
    
    } 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<meta name="robots" content="noindex,nofollow" />
			<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">

            <title>Agro :interface de connexion</title>
            

<!--		import Stylesheet		-->
<link href="default/css/style.css" rel="stylesheet" type="text/css"/>
<link href="default/css/blue.css" rel="stylesheet" type="text/css"/>
<link href="default/css/invalid.css" rel="stylesheet" type="text/css"/>


<!--		Import jQuery		-->
<script type="text/javascript" src="default/js/jquery-1.4.2.min.js"></script><!--import jquery-->
<script type="text/javascript" src="default/js/jquery.tools.min.js"></script> <!--Import jquery tools-->


<!--		Import Scripts		-->
<script type="text/javascript" src="default/js/script.js"></script>


<!--		Import Date picker		-->
<script type="text/javascript" src="default/js/jCal.js"></script>
<link href="default/css/jCal.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="default/js/jquery.animate.clip.js"></script>



<!--		Import Flot for charting		-->
<script type="text/javascript" src="default/js/jquery.flot.js"></script>

<!--		Import wysiwyg editor		-->
<script type="text/javascript" src="default/js/jquery.wysiwyg.js"></script>
<link rel="stylesheet" type="text/css" href="default/css/jquery.wysiwyg.css"/>


<!--		jExpand for Expanding Tables		-->
<script type="text/javascript" src="default/js/jExpand.js"></script>


<!--		Data Tables		-->
<script type="text/javascript" src="default/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="default/css/data_table.css"/>


<!--[if IE]><script language="javascript" type="text/javascript" src="default/js/excanvas.min.js"></script><![endif]--> 

<!--		PNG Fix and other fixes for ie6		-->
<!--[if lt IE 7]>
            <script type="text/javascript" src="default/js/dd_png_fix.js"></script>
            <script>DD_belatedPNG.fix('img, div, a');</script>
            <link href="css/ie.css" rel="stylesheet" type="text/css"  />
			<script language="javascript" type="text/javascript" src="../excanvas.min.js"></script>
<![endif]-->


</head>


<body id="login_page">

<div class="wrapper content">
<div <?php if(isset($erreur)) { echo 'class="error"'; } else { echo 'class="info"';} ?> style="height:30px">
	<?php
  if(isset($erreur))
  {
	echo $erreur; 
} else { ?>
    <strong><img src="default/images/iinfo_icon.png" alt="Information" width="28" height="29" class="icon" />Information :</strong> indiquez votre identifiant et votre mot de passe<a href="#" class="close_notification" title="Cliquez-ici pour fermer"><img src="default/images/close_icon.gif" width="6" height="6" alt="Fermer" /></a>
<?php } ?>
  </div>
 <div class="box">
    <div class="header">
      <p><img src="default/images/half_width_icon.png" alt="Interface de connexion" width="30" height="30" />Interface de connexion</p>
    </div>
    <div class="body">
      <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" style="margin-top:-30px">
        <p>
          <label>Identifiant :</label>
          <input name="FORM_LOGIN" type="text" class="textfield large" id="user_name" />
        </p>
        <p>
          <label>Mot de passe :</label>
          <input name="FORM_PASSWORD" type="password" class="textfield large" id="user_pass" />
        </p>
        <p>
                <input name="LOGIN_FORM_ENVOYER" type="submit" class="button2" value="Se connecter" />
                <input name="identification" type="hidden" value="1" /> <div align="right" style="margin-top:-40px"></div>
        </p>
      </form>
    </div>
  </div>  
</div>
</body>
</html>