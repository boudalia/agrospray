<?php
$svr=new SocketServer("chatserver.myconcept.local");
//Si serveur pr�t on installe les listeners et on d�marre le service
if(isset($svr)){
    function onAppStart($e){
        echo "Application is running";
    }
    function onAppStop($e){
        echo "Application is stopped";
    }
    function onConnect($e){
       echo "A new client is online ".$e->datas;
       $svr->broadCast($e->_target,"@SERVER : ".$e->_target->link_infos["host"]." is in the place... Make some noise !");
       $e->_target->send(str_replace("%USER%",$e->_target->link_infos["host"],$svr->wMsg));
    }
    function onDisconnect($e){
        echo "A client is disconnected (#".$e->datas["id"].")";
        $svr->broadCast(false,"@SERVER : ".$e->datas["link_infos"]["host"]." is gone !".">",true);
    }
    function onCall($e){
        echo "New Message";
        $svr->broadCast($e->_target,$e->_target->link_infos["host"]." > ".$e->datas.">",true);
        $e->_target->send(">");
        echo "Message broadcasted";
    }
    $svr->addEventListener(SocketServerEvent::APP_START ,"onAppStart");
    $svr->addEventListener(SocketServerEvent::APP_STOP  ,"onAppStop");
    $svr->addEventListener(SocketServerEvent::CONNECT   ,"onConnect");
    $svr->addEventListener(SocketServerEvent::DISCONNECT,"onDisconnect");
    $svr->addEventListener(SocketServerEvent::CALL      ,"onCall");
    echo "Server Ready";
    $message=array();
    $message[]="#############################################";
    $message[]=">>";
    $message[]=">>              MY TELNET CHAT SERVER";
    $message[]=">>";
    $message[]="#############################################";
    $message[]="";
    $message[]="Welcome %USER%";
    $message[]=">";
    $svr->wMsg=implode(PHP_EOL,$message);
    try{
        $svr->start();
    }
    catch(Exception $e){
        echo "Erreur lors du lancement du service";
    }
}